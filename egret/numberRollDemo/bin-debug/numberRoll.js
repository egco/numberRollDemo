var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var numberRoll = (function (_super) {
    __extends(numberRoll, _super);
    function numberRoll() {
        var _this = _super.call(this) || this;
        _this.skinName = 'numberRollSkin';
        _this.addEventListener(egret.Event.ADDED_TO_STAGE, _this.addToStage, _this);
        return _this;
    }
    numberRoll.prototype.addToStage = function (e) {
        this.startBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.startHandler, this);
    };
    numberRoll.prototype.startHandler = function (e) {
        egret.Tween.removeTweens(this);
        egret.Tween.get(this).to({ lab: Number(this.inputText.text) }, 200);
    };
    Object.defineProperty(numberRoll.prototype, "lab", {
        get: function () {
            return Number(this.numberLab.text);
        },
        set: function (text) {
            this.numberLab.text = '' + Math.floor(text);
        },
        enumerable: true,
        configurable: true
    });
    return numberRoll;
}(eui.Component));
__reflect(numberRoll.prototype, "numberRoll");
//# sourceMappingURL=numberRoll.js.map