class numberRoll extends eui.Component {
	private startBtn: eui.Button;
	private inputText: eui.TextInput;
	private numberLab: eui.Label;
	public constructor() {
		super();
		this.skinName = 'numberRollSkin';
		this.addEventListener(egret.Event.ADDED_TO_STAGE, this.addToStage, this);
	}
	private addToStage(e: egret.Event) {
		this.startBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.startHandler, this);
	}
	private startHandler(e: egret.TouchEvent) {
		egret.Tween.removeTweens(this);
		egret.Tween.get(this).to({lab: Number(this.inputText.text)}, 200);
	}
	private get lab(): number {
		return Number(this.numberLab.text);
	}
	private set lab(text) {
		this.numberLab.text = ''+Math.floor(text);
	}
}