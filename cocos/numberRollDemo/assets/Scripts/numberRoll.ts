const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property({ type: cc.Label, tooltip: '滚动标签' })
    numberLab: cc.Label = null;

    @property({ type: cc.EditBox, tooltip: '输入框' })
    editBox: cc.EditBox = null;
    numTw: cc.Tween = null;

    onLoad() {}
    start() {}
    
    rollHandler(event: cc.Node.EventType) {
        if(this.numTw) {
            this.numTw.stop();
        }
        this.numTw = cc.tween(this).to(0.2, {lab: Number(this.editBox.textLabel.string)}).start();
    }
    get lab(): number {
        return Number(this.numberLab.string);
    }
    set lab(text: number) {
        this.numberLab.string = '' + Math.floor(text);
    }
}
